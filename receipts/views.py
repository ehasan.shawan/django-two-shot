from django.shortcuts import render, redirect
from receipts.forms import ReceiptForm, CreateForm, CreateAccount
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required

# Create your views here.


# def receipts_list(request):
#     receipts = Receipt.objects.all()
#     context = {
#         "receipts_list": receipts,
#     }
#     return render(request, "receipts/receipts.html", context)


@login_required
def receipts_list(request):

    receipts = Receipt.objects.filter(purchaser=request.user)

    context = {
        "receipts_list": receipts
    }
    return render(request, "receipts/receipts.html", context)


@login_required
def create(request):

    if request.method == "POST":

        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()

            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


def category_list(request):

    category = ExpenseCategory.objects.filter(owner=request.user)

    context = {
        "category_list": category
    }
    return render(request, "receipts/categories.html", context)


def account_list(request):

    account = Account.objects.filter(owner=request.user)

    context = {
        "account_list": account
    }
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):

    if request.method == "POST":

        form = CreateForm(request.POST)
        if form.is_valid():
            create = form.save(False)
            create.owner = request.user
            create.save()

            return redirect("category_list")
    else:
        form = CreateForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):

    if request.method == "POST":

        form = CreateAccount(request.POST)
        if form.is_valid():
            create = form.save(False)
            create.owner = request.user
            create.save()

            return redirect("account_list")
    else:
        form = CreateAccount()

    context = {
        "form": form,
    }
    return render(request, "receipts/accounts/create.html", context)
