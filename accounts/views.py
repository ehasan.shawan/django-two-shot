from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from accounts.forms import SignInForm, SignUpForm

# Create your views here.


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            # Creating a User with these values
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,

                )
                # Logging in the User
                login(request, user)
                return redirect("home")
            else:
                form.add_error("password",
                               "Passwords do not match")

    else:
        # If request method is not POST, show an empty form
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)


def user_login(request):
    if request.method == "POST":
        form = SignInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = SignInForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def log_out(request):
    logout(request)
    return redirect("login")
