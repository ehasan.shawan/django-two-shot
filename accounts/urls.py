from django.urls import path
from accounts.views import user_login, log_out, signup


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", log_out, name="logout"),
    path("signup/", signup, name="signup"),

]
